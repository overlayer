#!/bin/bash

# include global header
HEADERS="headers.include"
if [ ! -e $HEADERS ]; then
    echo "[!] error loading global defines $HEADERS"
	exit 1
else
    . $HEADERS
fi

# include helper functions
if [ ! -e $FUNCTIONS ]; then
    echo "[!] error loading helper functions $FUNCTIONS"
	exit
else
    . $FUNCTIONS
fi

log_info "umounting special dirs ..."
umount $WORKDIRROOT/dev
umount $WORKDIRROOT/proc

log_info "removing overlays ..."
umount $WORKDIRISO
umount $WORKDIRROOT

log_info "chroot env cleaned ..."

