#!/bin/bash

# include global header
HEADERS="headers.include"
if [ ! -e $HEADERS ]; then
    echo "[!] error loading global defines $HEADERS"
	exit 1
else
    . $HEADERS
fi

# include helper functions
if [ ! -e $FUNCTIONS ]; then
    echo "[!] error loading helper functions $FUNCTIONS"
	exit
else
    . $FUNCTIONS
fi

log_info "preparing overlays ..."
mount -t unionfs -o dirs=$SNAPISO=rw:$OVERLAYISO=ro:$UNPACKDIRISO=ro \
						 none $WORKDIRISO
mount -t unionfs -o dirs=$SNAPROOT=rw:$OVERLAYROOT=ro:$UNPACKDIRROOT=ro \
						 none $WORKDIRROOT

log_info "mounting special dirs ..."
mount -o bind /dev $WORKDIRROOT/dev
mount -t proc none $WORKDIRROOT/proc

log_info "you can chroot to $WORKDIRROOT now ..."

