#!/bin/bash
# desc: init inside

# include helper functions
FUNCS="/tmp/functions.include"
if [ ! -e $FUNCS ]; then
    echo "[!] Error loading helper functions '$FUNCS'"
	exit
else
    . $FUNCS
fi

log_info "  updating ld cache ..."
ldconfig

