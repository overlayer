#!/bin/bash
# desc: update apt cache

# include helper functions
FUNCS="/tmp/functions.include"
PACKETLOG="/tmp/debs.log"
if [ ! -e $FUNCS ]; then
    echo "[!] Error loading helper functions '$FUNCS'"
	exit
else
    . $FUNCS
fi

# configs are in '/'
CONFDIR="/"

# tmp must be writable
chmod 777 /tmp

log_info "  updating apt cache ..."
apt-get update >>$PACKETLOG 2>&1
if [ $? != 0 ]; then
    log_warn "  some warnings occured while updating"
    log_warn "  check (chroot)$PACKETLOG for details"
fi

log_info "  checking package dependencies ..."
apt-get -y -f install >>$PACKETLOG 2>&1
if [ $? != 0 ]; then
    log_warn "  some warnings occured while checking deps"
    log_warn "  check (chroot)$PACKETLOG for details"
fi

log_info "  removing unwanted packages ((chroot)$PACKETLOG) ..."
apt-get --purge -y remove $(cat $CONFDIR/packages-remove) \
        >>$PACKETLOG 2>&1
if [ $? != 0 ]; then
    log_warn "  some warnings occured while removing packages"
    log_warn "  check (chroot)$PACKETLOG for details"
fi

# add additional software
log_info "  installing packages ((chroot)$PACKETLOG) ..."
apt-get -y install $(cat $CONFDIR/packages-install) \
        >>$PACKETLOG 2>&1
if [ $? != 0 ]; then
    log_warn "  some warnings occured while installing packages"
    log_warn "  check (chroot)$PACKETLOG for details"
fi

# remove orphaned packages
# WARN: use with caution
#log_info "  removing orphans ..."
#deborphan | xargs apt-get remove

# final? remove cache
# TODO: this can be done with new overlay
#       does not work atm, since TARGET unknown here
if [ "$TARGET" == "final" ]; then
    log_info "  cleaning apt cache ..."
    apt-get clean
fi

