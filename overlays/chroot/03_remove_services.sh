#!/bin/bash
# desc: remove unwanted services from runlevels

# include helper functions
FUNCS="/tmp/functions.include"
PACKETLOG="/tmp/debs.log"
CONFDIR="/"
if [ ! -e $FUNCS ]; then
    echo "  [!] error loading helper functions $FUNCS"
	exit
else
    . $FUNCS
fi
if [ ! -e $CONFDIR/services-remove ]; then
    err "  error loading service configs"
	exit
else
    . $CONFDIR/services-remove
fi

log_info "  removing services from runlevels ..."
for service in $SERVICES; do
    /etc/init.d/$service stop >/dev/null 2>&1
    update-rc.d -f $service remove >/dev/null 2>&1
	if [ $? != 0 ]; then
	    log_warn "  could not remove $service"
	else
	    log_info "  removed service $service"
	fi
done

