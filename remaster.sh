#!/bin/bash
#
# overlayer - Automated Knoppix Remastering Worksuite
# Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

# include global header
HEADERS="headers.include"
if [ ! -e $HEADERS ]; then
    echo "[!] error loading global defines $HEADERS"
	exit 1
else
    . $HEADERS
fi

# include helper functions
if [ ! -e $FUNCTIONS ]; then
    echo "[!] error loading helper functions $FUNCTIONS"
	exit
else
    . $FUNCTIONS
fi

# trap signals
cleanup()
{
    log_info "cleaning up ..."
    # umount possibly mounted stuff
    umount $LOOPDIR >/dev/null 2>&1
	umount /dev/cloop >/dev/null 2>&1
    # remove loaded modules
    rmmod cloop >/dev/null 2>&1
    # remove chroot mounts
    umount $WORKDIRROOT/dev >/dev/null 2>&1
    umount $WORKDIRROOT/proc >/dev/null 2>&1
    umount $WORKDIRROOT/var/cache/apt >/dev/null 2>&1
    umount $WORKDIRROOT/var/lib/apt >/dev/null 2>&1
    # remove overlay mount
	remove_overlays
    log_info "cleanup done ... quitting"
    exit 1
}
trap cleanup 1 2 3 6

# greetings
log_info "*** overlayer - Automated Knoppix Remastering Worksuite ***"
log_info "starting up ..."

# assign target if any given
TARGET=$1
# debug?
if [ $DEBUG -eq 1 ]; then
    log_debug "debug output enabled"
fi

# read in the scripts to execute
numscript=0
log_debug "loading scripts from $SCRIPTDIR"
for i in `ls $SCRIPTDIR/[0-9][0-9]_*`; do
	SCRIPTS[$numscript]=$i
    log_debug "loaded script ${SCRIPTS[$numscript]} into memory"
	((numscript = numscript + 1))
done
log_info "loaded $numscript scripts ..."


# for now we just execute all scripts
# if one of them fails, exit with non zero
# exit status
for ((i=0; $i < $numscript; i++)) {
	log_debug "executing script [$i] -> ${SCRIPTS[$i]}"
	.  ${SCRIPTS[$i]}
	if [ $? != 0 ]; then
		err "script ${SCRIPTS[$i]} had non-zero exit status" 1
	fi
}

exit 0

