#!/bin/bash
# desc: execute chroot scripts in $CHROOTDIR

TMPDIR="$WORKDIRROOT/tmp"

cp $FUNCTIONS $TMPDIR

# mount necessary stuff
mount --bind /dev $WORKDIRROOT/dev >/dev/null 2>&1
mount -t proc none $WORKDIRROOT/proc >/dev/null 2>&1
# add modular overlays
add_root_overlay $OVERLAYCONF
add_root_overlay $OVERLAYCHROOT
flush

# make networking work
rm -f $WORKDIR/etc/dhcpc/resolv.conf
cp /etc/resolv.conf $WORKDIRROOT/etc/dhcpc/resolv.conf

log_info "executing chroot scripts from $WORKDIRROOT ..."
for z in `ls $WORKDIRROOT/[0-9][0-9]_*`; do
    script=`basename $z`
    ( chroot "$WORKDIRROOT" "./$script" )
done
flush 4

umount $WORKDIRROOT/dev >/dev/null 2>&1
umount $WORKDIRROOT/proc >/dev/null 2>&1
remove_root_overlay $OVERLAYCONF
remove_root_overlay $OVERLAYCHROOT

