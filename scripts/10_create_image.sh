#!/bin/bash
# desc: create release ISO

# flush disk caches
flush

if [ "$TARGET" != "final" ]; then
    log_warn "not creating image, TARGET is not 'final'"
    return
fi

if [ "`id -u`" != 0 ]; then
    err "must be root to run this script" 0
fi
if [ -z "$BUILDTARGET" ]; then
    err "no release name received" 1
fi

if [ ! -d "$WORKDIRISO" ]; then
    err "$WORKDIRISO is missing" 1
fi
if [ ! -d "$WORKDIRROOT" ]; then
    err "$WORKDIRROOT is missing" 1
fi

# local vars
CLOOPIMAGE="$WORKDIRISO/KNOPPIX/KNOPPIX"
log_info "creating $CLOOPIMAGE ... this may take a while"
mkisofs -R -U -V "KNOPPIX.net filesystem" -publisher "$PUBLISHER" \
    -input-charset ISO-8859-1 -hide-rr-moved -cache-inodes -no-bak \
	-pad "$WORKDIRROOT" 2>>$BUILDLOG \
	| nice -5 create_compressed_fs - 65536 > "$CLOOPIMAGE" 2>>$BUILDLOG
if [ $? != 0 ]; then
    err "creating $CLOOPIMAGE failed" 1
fi
log_info "creating $BUILDTARGET"
mkisofs -r -J -b boot/isolinux/isolinux.bin \
  -c boot/isolinux/boot.cat \
  -no-emul-boot -boot-load-size 4 -boot-info-table \
  -o "$BUILDTARGET" "$WORKDIRISO" >>$BUILDLOG 2>&1
if [ $? != 0 ] ; then
    err "creating $BUILDTARGET failed" 1
fi

log_info "$BUILDTARGET created"

