#!/bin/bash
# desc: remove files

PATCHES=$OVERLAYPATCHES

log_info "patching files ..."

for patch in `ls $PATCHES`; do
    cd $WORKDIRROOT
    patch -N -p0 < $PATCHES/$patch >/dev/null
    if [ $? != 0 ]; then
        log_warn "  could not apply patch $patch (already patched?)"
    else
        log_info "  applied patch $patch"
    fi
done
cd $ROOT

