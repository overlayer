#!/bin/bash
# desc: remove files

CONF=$OVERLAYCONF/mod-files
if [ ! -e $CONF ]; then
    err "error loading config $CONF"
	exit 1
else
    . $CONF
fi

log_info "deleting unwanted files ..."

for f in $FILES; do
    log_debug "deleting: $f"
	rm -r $f
	if [ $? != 0 ]; then
	    log_warn "could not delete $f"
	fi
done

