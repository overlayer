#!/bin/bash
# desc: image unpack helper

if [ "`id -u`" != 0 ] ; then
    err "must be root to run this script" 0
fi

# ceck iso
if [ -z "$ISOIMAGE" ] ; then
    err "no ISO to work with received" 1
fi
if [ ! -f "$ISOIMAGE" ] ; then
    err "the ISO $ISOIMAGE somehow disappeared" 1
fi

# local vars
CLOOPIMAGE="$UNPACKDIRISO/KNOPPIX/KNOPPIX"

# result already there? skip
if [ ! -d "$UNPACKDIRISO" ]; then
    mkdir -p $UNPACKDIRISO $LOOPDIR
    # mount iso, copy data
    mount -o loop,ro "$ISOIMAGE" "$LOOPDIR"
    if [ $? != 0 ] ; then
        err "mounting of $ISOIMAGE failed" 1
    fi
    log_info "copying ISO contents to $UNPACKDIRISO"
    (cd "$LOOPDIR" && cp -a * "$UNPACKDIRISO")
    if [ $? != 0 ] ; then
        err "copying of ISO contents failed" 1
    fi
    umount "$LOOPDIR"
else
    log_info "$UNPACKDIRISO already exists ... skipping"
fi

if [ ! -d "$UNPACKDIRROOT" ]; then
    mkdir -p $UNPACKDIRROOT
    modprobe cloop file=$CLOOPIMAGE
    if [ $? != 0 ] ; then
        err "could not modprobe cloop, maybe build it first?" 1
    fi

    mount -r /dev/cloop0 "$LOOPDIR"
    if [ $? != 0 ] ; then
        rmmod cloop >/dev/null 2>&1
        err "mounting of $CLOOPIMAGE failed" 1
    fi
    log_info "copying compressed contents to $UNPACKDIRROOT"
    (cd "$LOOPDIR" && cp -a . "$UNPACKDIRROOT")
    if [ $? != 0 ] ; then
        err "copying of compressed ISO contents failed" 1
    fi
    umount "$LOOPDIR"
    rmmod cloop
else
    log_info "$UNPACKDIRROOT already exists ... skipping"
fi

