#!/bin/bash
# desc: set overlays in place

log_info "placing overlays ..."

# pass the overlays to place_overlays()
# for handling
place_overlays "$OVERLAYISO" "$UNPACKDIRISO" \
                   "$WORKDIRISO" "$SNAPISO"
place_overlays "$OVERLAYROOT" "$UNPACKDIRROOT" \
                   "$WORKDIRROOT" "$SNAPROOT"

flush

# disable unionfs
# use rsync instead
# sync everything to $WORKDIR -> slow
#cd $UNPACKDIRISO; cp -a . $WORKDIRISO
#cd $OVERLAYISO; rsync -ar . $WORKDIRISO

#cd $UNPACKDIRROOT; cp -a . $WORKDIRROOT
#cd $OVERLAYROOT; rsync -ar . $WORKDIRROOT

