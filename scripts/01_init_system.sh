#!/bin/bash
# desc: check system, assign ISO

# must be root
if [ "`id -u`" != 0 ] ; then
    err "must be root to run this script" 0
fi

# check needed modules
modinfo unionfs >/dev/null 2>&1
if [ $? != 0 ]; then
    err "you need to build the unionfs module first" 0
    echo $out
fi
modinfo cloop >/dev/null 2>&1
if [ $? != 0 ]; then
    err "you need to build the cloop module first" 0
fi

# check for existence of *one* iso image
numimages=`ls $ISODIR/*.iso 2>/dev/null | wc -l`
if [ $numimages -ne 1 ]; then
    err "please place *one* ISO image in '$ISODIR'" 1
fi

# check if file is really an iso image
iso=`ls $ISODIR/*.iso`
file=`file $iso | grep "ISO 9660"`
if [ -z "$file" ]; then
    err "'$iso' is not ISO 9660" 1
fi

# buildtarget should not exits
if [ -f "$BUILDTARGET" ]; then
    err "$BUILDTARGET already exists" 1
fi

# assign iso
ISOIMAGE=$iso
log_info "using ISO $ISOIMAGE"

# create some needed dirs
mkdir -p $BUILD $SNAPROOT $SNAPISO
mkdir -p $WORKDIR $WORKDIRISO $WORKDIRROOT
mkdir -p $UNPACKDIR

# purge logs
>$BUILDLOG

